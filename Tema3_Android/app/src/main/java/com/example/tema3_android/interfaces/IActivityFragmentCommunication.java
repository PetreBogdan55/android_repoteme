package com.example.tema3_android.interfaces;

import com.example.tema3_android.models.Book;

public interface IActivityFragmentCommunication {
    void openFragment();
    void openBookFragment(Book book);
}
