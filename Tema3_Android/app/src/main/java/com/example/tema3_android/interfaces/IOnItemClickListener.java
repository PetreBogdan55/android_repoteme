package com.example.tema3_android.interfaces;

import com.example.tema3_android.models.Book;

public interface IOnItemClickListener {
    void openBookFragment(Book book);
    void deleteBook(Book book);
}
