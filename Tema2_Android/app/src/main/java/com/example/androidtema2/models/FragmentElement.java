package com.example.androidtema2.models;

public class FragmentElement {
    public FragmentElementType getFragmentElement() {
        return type;
    }

    FragmentElementType type;

    public FragmentElement(FragmentElementType type) {
        this.type = type;
    }
}
