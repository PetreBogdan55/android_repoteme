package com.example.androidtema2.data;

public class Data {
    public static final String BASE_URL = "https://jsonplaceholder.typicode.com";
    public static final String USER_ID = "userId";
    public static final String ALBUM_ID = "albumId";
    public static final String PLACEHOLDER_IMAGE_URL="https://picsum.photos/300";
}
