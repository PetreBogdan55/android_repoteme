package com.example.androidtema2.interfaces;

import com.example.androidtema2.models.FragmentElement;

public interface ActivityFragmentCommunication {
    void openFragment1();
    void openFragment2(FragmentElement user);
    void openFragment3(FragmentElement album);
}
