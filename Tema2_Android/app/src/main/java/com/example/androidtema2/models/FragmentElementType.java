package com.example.androidtema2.models;

public enum FragmentElementType {
    USER,
    POST,
    ALBUM,
    IMAGE
}
