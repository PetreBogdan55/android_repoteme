package com.example.androidtema2.models;

public class Album extends FragmentElement {

    private int id;
    private final String body;

    public Album(int id, String body) {
        super(FragmentElementType.ALBUM);
        this.id = id;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

}
