package com.example.androidtema2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.androidtema2.R;
import com.example.androidtema2.data.Data;
import com.example.androidtema2.interfaces.OnItemClickListener;
import com.example.androidtema2.models.Album;
import com.example.androidtema2.models.FragmentElement;
import com.example.androidtema2.models.Image;
import com.example.androidtema2.models.Post;
import com.example.androidtema2.models.User;
import com.example.androidtema2.volley.VolleyConfigSingleton;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<FragmentElement> listOfFragmentElements;
    OnItemClickListener onItemClickListener;

    public MyAdapter(ArrayList<FragmentElement> listOfFragmentElements, OnItemClickListener onItemClickListener) {
        this.listOfFragmentElements = listOfFragmentElements;
        this.onItemClickListener = onItemClickListener;
    }

    public MyAdapter(ArrayList<FragmentElement> listOfFragmentElements) {
        this.listOfFragmentElements = listOfFragmentElements;
    }

    @Override
    public int getItemViewType(int position) {
        switch(listOfFragmentElements.get(position).getFragmentElement().toString())
        {
            case "ARTIST":
                return 0;
            case "POST":
                return 1;
            case "ALBUM":
                return 2;
            case "IMAGE":
                return 3;
            default:
                return super.getItemViewType(position);
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch(viewType)
        {
            case 0:
                view = inflater.inflate(R.layout.item_user, parent, false);
                return new ArtistViewHolder(view);
            case 1:
                view = inflater.inflate(R.layout.item_posts, parent, false);
                return new PostsHolder(view);
            case 2:
                view = inflater.inflate(R.layout.item_album, parent, false);
                return new AlbumHolder(view);
            case 3:
                view = inflater.inflate(R.layout.item_photos, parent, false);
                return new PhotosHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArtistViewHolder) {
            User user = (User) listOfFragmentElements.get(position);
            ((ArtistViewHolder) holder).bind(user);
        } else if (holder instanceof PostsHolder) {
            Post post = (Post) listOfFragmentElements.get(position);
            ((PostsHolder) holder).bind(post);
        } else if (holder instanceof AlbumHolder) {
            Album album = (Album) listOfFragmentElements.get(position);
            ((AlbumHolder) holder).bind(album);
        } else if (holder instanceof PhotosHolder) {
            Image image = (Image) listOfFragmentElements.get(position);
            ((PhotosHolder) holder).bind(image);
        }
    }

    @Override
    public int getItemCount() {
        return this.listOfFragmentElements.size();
    }

    class ArtistViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView username;
        private final TextView email;
        private final ImageButton imageButton;
        private final View view;

        ArtistViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            username = view.findViewById(R.id.username);
            email = view.findViewById(R.id.email);
            imageButton = view.findViewById(R.id.imageView);
            this.view = view;
        }


        void bind(User user) {
            name.setText(user.getName());
            username.setText(user.getUsername());
            email.setText(user.getEmail());
            imageButton.setOnClickListener(
                    v -> {
                        if (onItemClickListener != null) {
                            onItemClickListener.onImageClick(user);
                            notifyItemChanged(getAdapterPosition());
                        }
                    });

            view.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(user);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }

    static class PostsHolder extends RecyclerView.ViewHolder {
        private final TextView title;
        private final TextView body;

        PostsHolder(View view) {
            super(view);
            title = view.findViewById(R.id.post_title);
            body = view.findViewById(R.id.body);
        }

        void bind(Post post) {
            title.setText(post.getTitle());
            body.setText(post.getBody());
        }
    }


    class AlbumHolder extends RecyclerView.ViewHolder {
        private final TextView body;
        private final View view;

        public AlbumHolder(@NonNull View view) {
            super(view);
            body = view.findViewById(R.id.album_body);
            this.view = view;
        }

        public void bind(Album album) {
            body.setText(album.getBody());
            view.setOnClickListener(v -> onItemClickListener.onItemClick(album));
        }
    }

    static class PhotosHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;

        public PhotosHolder(@NonNull View view) {
            super(view);
            imageView = view.findViewById(R.id.photos_body);
        }

        public void bind(Image image) {
            String imageUrl = Data.PLACEHOLDER_IMAGE_URL; //image.getUrl();
            ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();
            imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    // PHOTO ERROR
                }
            });

        }
    }
}