package com.example.androidtema2.interfaces;

import com.example.androidtema2.models.FragmentElement;
import com.example.androidtema2.models.User;

public interface OnItemClickListener {
    void onItemClick(FragmentElement fragmentElement);
    void onImageClick(User user);
}
