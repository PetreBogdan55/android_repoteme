package com.example.tema1_android.activities;

import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import android.view.SurfaceControl;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.tema1_android.R;
import com.example.tema1_android.fragments.F1A2;
import com.example.tema1_android.fragments.F2A2;
import com.example.tema1_android.fragments.F3A2;
import com.example.tema1_android.interfaces.ActivityFragmentCommunication;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public class MainActivity2 extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);
        addF1A2();
    }

    public void addF1A2() {

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F1A2.class.getName();
        FragmentTransaction addTransaction = transaction.add(
                R.id.frame_layout, F1A2.newInstance("",""), tag
        );
        addTransaction.addToBackStack(tag);
        addTransaction.commit();
    }
    @Override
    public void openSecondActivity() {

    }

    @Override
    public void replaceF2A2withF3A2() {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F3A2.class.getName();
        FragmentTransaction replaceTransaction = transaction.add(
                R.id.frame_layout, F3A2.newInstance("",""), tag
        );
        replaceTransaction.addToBackStack(null);
        replaceTransaction.commit();
    }

    @Override
    public void returnToF1A2() {

    }

    @Override
    public void closeActivity2() {
        finish();
    }

    @Override
    public void returnToF1A2Button() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void replaceWithF2A2() {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F2A2.class.getName();
        FragmentTransaction replaceTransaction = transaction.add(
                R.id.frame_layout, F2A2.newInstance("",""), tag
        );
        replaceTransaction.addToBackStack(null);
        replaceTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
