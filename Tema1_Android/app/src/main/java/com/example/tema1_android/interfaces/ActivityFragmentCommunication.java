package com.example.tema1_android.interfaces;

public interface ActivityFragmentCommunication {
    void openSecondActivity();
    void replaceF2A2withF3A2();
    void closeActivity2();
    void returnToF1A2();
    void returnToF1A2Button();
    void replaceWithF2A2();
}
