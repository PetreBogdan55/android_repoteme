package com.example.tema1_android.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.tema1_android.R;
import com.example.tema1_android.interfaces.ActivityFragmentCommunication;

public class MainActivity extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void openSecondActivity() {
        Intent intent=new Intent(this,MainActivity2.class);
        startActivity(intent);
    }

    @Override
    public void replaceF2A2withF3A2() {

    }

    @Override
    public void closeActivity2() {

    }

    @Override
    public void returnToF1A2() {

    }

    @Override
    public void returnToF1A2Button() {

    }

    @Override
    public void replaceWithF2A2() {

    }
}